const PostgresStore = require('../utils/PostgresStore.js');

class Todo {
  static toSqlTable () {
    return `
            CREATE TABLE ${Todo.tableName} (
                -- id, firstname, lastname, email, password
                id SERIAL PRIMARY KEY,
                name VARCHAR(100)
            )
        `;
  }

  static async getAllTodos () {
    const result = await PostgresStore.client.query({
      text: `
                  SELECT * FROM ${Todo.tableName}`
    });
    return result;
  }

  static async addTodo (todoName) {
    const result = await PostgresStore.client.query({
      text: `
                  INSERT INTO ${Todo.tableName} (name) VALUES ($1)`,
      values: [todoName]
    });
    return result;
  }

  static async deleteTodo (todoName) {
    const result = await PostgresStore.client.query({
      text: `
                  DELETE FROM ${Todo.tableName} WHERE name = $1`,
      values: [todoName]
    });
    return result;
  }
}

/** @type {String} **/
Todo.tableName = 'Todo';

module.exports = Todo
;
