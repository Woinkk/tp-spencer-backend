var express = require('express');
var todo = require('../models/todo.model.js');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/getAll', async function (req, res, next) {
  const myTodos = await todo.getAllTodos();
  res.json(myTodos.rows);
});

router.get('/add', function (req, res, next) {
  const query = req.query;
  todo.addTodo(query.todo);
  res.sendStatus(200);
});

router.get('/delete', function (req, res, next) {
  const query = req.query;
  todo.deleteTodo(query.todo);
  res.sendStatus(200);
});

module.exports = router;
